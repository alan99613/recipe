package com.example.recipe;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class UserTest {

    @Test
    public void getToken() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        User user = new User(appContext);
        user.setToken("test");
        assertEquals("Bearer test", user.getToken());
    }

    @Test
    public void removeToken() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        User user = new User(appContext);
        user.setToken("test");
        user.removeToken();
        assertEquals("", user.getToken());
    }
}