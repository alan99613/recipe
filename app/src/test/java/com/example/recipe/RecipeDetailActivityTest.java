package com.example.recipe;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.*;

public class RecipeDetailActivityTest {

    @Test
    public void jsonArrayToArray() {
        RecipeDetailActivity recipeDetailActivity = new RecipeDetailActivity();
        String jsonString = "{\"steps\":[\"a\",\"b\",\"c\"]}";
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            String[] jsonArray = { "a", "b", "c" };
            assertArrayEquals(recipeDetailActivity.jsonArrayToArray(jsonObject, "steps"), jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}