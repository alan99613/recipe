package com.example.recipe;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.common.model.LocalModel;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.label.ImageLabel;
import com.google.mlkit.vision.label.ImageLabeler;
import com.google.mlkit.vision.label.ImageLabeling;
import com.google.mlkit.vision.label.custom.CustomImageLabelerOptions;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class IdentificationActivity extends AppCompatActivity {
    private final int CAM = 10;
    private final int IMG = 11;
    private ImageView identificationImage;
    private ListView identificationResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identification);

        identificationImage = findViewById(R.id.identificationImage);
        identificationResult = findViewById(R.id.identificationResult);
    }

    public void takePicture(View view) {
        //Check if camera permission has been obtained
        if (ContextCompat.checkSelfPermission(IdentificationActivity.this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(IdentificationActivity.this, new String[]{
                    Manifest.permission.CAMERA
            }, CAM);
        } else {
            turnOnCamera();
        }
    }

    //Check if the camera permission has been obtained after asking the user
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAM) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                turnOnCamera();
            }
        }
    }

    public void selectPicture(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMG);
    }

    private void turnOnCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAM);
    }

    public void generateLabels(Bitmap bitmap) {
        InputImage image = InputImage.fromBitmap(bitmap, 0);
        //Use local model
        LocalModel localModel = new LocalModel.Builder()
                .setAssetFilePath("lite-model_aiy_vision_classifier_food_V1_1.tflite")
                .build();
        CustomImageLabelerOptions customImageLabelerOptions =
                new CustomImageLabelerOptions.Builder(localModel)
                        .setMaxResultCount(5)
                        .setConfidenceThreshold(0.01f)
                        .build();
        ImageLabeler labeler = ImageLabeling.getClient(customImageLabelerOptions);
        labeler.process(image).addOnSuccessListener(new OnSuccessListener<List<ImageLabel>>() {
            @Override
            public void onSuccess(@NonNull List<ImageLabel> imageLabels) {
                List<String> resultList = new ArrayList<>();
                DecimalFormat df = new DecimalFormat("#.##");
                for (ImageLabel label: imageLabels) {
                    String text = label.getText();
                    float confidence = label.getConfidence();
                    resultList.add(text + ": " + df.format(confidence * 100.0f) + "%");
                }

                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                        IdentificationActivity.this,
                        android.R.layout.simple_list_item_1,
                        resultList);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(IdentificationActivity.this, getString(R.string.longPressHints), Toast.LENGTH_LONG).show();
                        identificationResult.setAdapter(arrayAdapter);
                        identificationResult.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                            @Override
                            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                                String text = (String) identificationResult.getItemAtPosition(position);
                                text = text.replaceAll(":.+$", ""); //Remove the text after the colon
                                //Copy the food name to clipboard
                                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                ClipData clip = ClipData.newPlainText(getString(R.string.foodName), text);
                                clipboard.setPrimaryClip(clip);
                                Toast.makeText(IdentificationActivity.this, getString(R.string.copied), Toast.LENGTH_LONG).show();
                                return true;
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == CAM) { //Get photos taken by the camera
            if (data != null) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                identificationImage.setImageBitmap(bitmap);
                generateLabels(bitmap);
            }
        } else if (requestCode == IMG) { //Get the photo selected by the user
            try {
                if (data != null) {
                    Uri selectedImage = data.getData();
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    identificationImage.setImageBitmap(bitmap);
                    generateLabels(bitmap);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}