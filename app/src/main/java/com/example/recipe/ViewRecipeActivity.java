package com.example.recipe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.recipe.adapter.RecipeAdapter;
import com.example.recipe.model.Recipe;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.connection.RealCall;

public class ViewRecipeActivity extends AppCompatActivity {
    private OkHttpClient client;
    private ListView resultListView;
    private EditText searchName;
    private Callback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_recipe);

        client = new OkHttpClient();
        resultListView = findViewById(R.id.recipeList);
        searchName = findViewById(R.id.searchName);
        callback = new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        List<Recipe> recipes = new LinkedList<>();
                        JSONArray recipeJson = new JSONArray(response.body().string());
                        for (int i = 0; i < recipeJson.length(); i++) {
                            String recipeID = recipeJson.getJSONObject(i).getString("_id");
                            String recipeName = recipeJson.getJSONObject(i).getString("recipeName");
                            String image = recipeJson.getJSONObject(i).getString("image");
                            recipes.add(new Recipe(recipeID, recipeName, image));
                        }
                        RecipeAdapter recipeAdapter = new RecipeAdapter(recipes, ViewRecipeActivity.this);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //Update the list view
                                resultListView.setAdapter(recipeAdapter);
                                resultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        Intent intent = new Intent(ViewRecipeActivity.this, RecipeDetailActivity.class);
                                        TextView recipeID = view.findViewById(R.id.recipe_child_id);
                                        intent.putExtra("recipeID", recipeID.getText().toString());
                                        startActivity(intent);
                                    }
                                });
                            }
                        });
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 400) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ViewRecipeActivity.this, getString(R.string.noResult), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
            }
        };

        getRecipe("https://alan-recipe-api.herokuapp.com/api/v1/recipe?num=" + 8, callback);
    }

    public void searchRecipe(View view) {
        String keyword = searchName.getText().toString();
        if (keyword.equals("")) {
            Toast.makeText(ViewRecipeActivity.this, getString(R.string.search_hints), Toast.LENGTH_SHORT).show();
        } else {
            getRecipe("https://alan-recipe-api.herokuapp.com/api/v1/recipe/name/" + keyword, callback);
        }
    }

    private void getRecipe(String url, Callback callback) {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }
}