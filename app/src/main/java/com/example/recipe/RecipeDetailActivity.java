package com.example.recipe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.recipe.model.Recipe;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RecipeDetailActivity extends AppCompatActivity {
    private OkHttpClient client;
    private User user;
    private Button favouriteButton;
    private boolean favourite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);

        favourite = false;
        client = new OkHttpClient();
        user = new User(this);
        favouriteButton = findViewById(R.id.favouriteButton);
        String recipeID = getIntent().getStringExtra("recipeID");
        if (recipeID != null) {
            getRecipeDetail(recipeID);

            //Check if the user is logged in
            if (user.getToken().equals(""))
                favouriteButton.setVisibility(View.GONE);
            else
                checkFavourite(recipeID);
            favouriteButton.setOnClickListener(v -> handleFavourite(!favourite, recipeID));
        }
    }

    private void getRecipeDetail(String recipeID) {
        Request request = new Request.Builder()
                .url("https://alan-recipe-api.herokuapp.com/api/v1/recipe/id/" + recipeID)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                try {
                    JSONObject recipeJson = new JSONObject(response.body().string());
                    String recipeID = recipeJson.getString("_id");
                    String recipeName = recipeJson.getString("recipeName");
                    String recipeDescription = recipeJson.getString("description");
                    String imageUrl = recipeJson.getString("image");
                    String[] ingredients = jsonArrayToArray(recipeJson, "ingredients");
                    String[] steps = jsonArrayToArray(recipeJson, "steps");
                    Recipe recipe = new Recipe(recipeID, recipeName, recipeDescription, imageUrl, ingredients, steps);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateUI(recipe);
                        }
                    });
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
            }
        });
    }

    private void updateUI(Recipe recipe) {
        TextView detailRecipeName = findViewById(R.id.detailRecipeName);
        ImageView detailRecipeImage = findViewById(R.id.detailRecipeImage);
        TextView detailRecipeDescription = findViewById(R.id.detailRecipeDescription);
        TextView detailRecipeIngredients = findViewById(R.id.detailRecipeIngredients);
        TextView detailRecipeSteps = findViewById(R.id.detailRecipeSteps);

        detailRecipeName.setText(recipe.getRecipeName());
        Picasso.get().load(recipe.getImageUrl()).into(detailRecipeImage);
        detailRecipeDescription.setText(recipe.getDescription());
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < recipe.getIngredients().length; i++) {
            stringBuffer.append(recipe.getIngredients()[i]).append("\n");
        }
        detailRecipeIngredients.setText(stringBuffer);
        stringBuffer = new StringBuffer();
        for (int i = 0; i < recipe.getSteps().length; i++) {
            stringBuffer.append(i + 1).append(". ").append(recipe.getSteps()[i]).append("\n");
        }
        detailRecipeSteps.setText(stringBuffer);
    }

    public String[] jsonArrayToArray(JSONObject jsonObject, String name) {
        try {
            JSONArray jsonArray = jsonObject.getJSONArray(name);
            String[] array = new String[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++){
                array[i] = jsonArray.getString(i);
            }
            return array;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Check if the recipe is in the favourites
    private void checkFavourite(String recipeID) {
        Request request = new Request.Builder()
                .url("https://alan-recipe-api.herokuapp.com/api/v1/favourite?id="+recipeID)
                .addHeader("Authorization", user.getToken())
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject resultJson = new JSONObject(response.body().string());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (resultJson.getBoolean("isFavourite")) {
                                        favourite = true;
                                        favouriteButton.setText(R.string.removeFavourites);
                                    } else {
                                        favourite = false;
                                        favouriteButton.setText(R.string.addFavourites);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 401) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            user.removeToken();
                            Toast.makeText(RecipeDetailActivity.this, getString(R.string.authFailed), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(RecipeDetailActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
            }
        });
    }

    //Add or delete recipes from favourites
    private void handleFavourite(boolean add, String recipeID) {
        Request request;
        if (add) {
            RequestBody body = RequestBody.create(new byte[0], null);
            request = new Request.Builder()
                    .url("https://alan-recipe-api.herokuapp.com/api/v1/favourite/"+recipeID)
                    .addHeader("Authorization", user.getToken())
                    .put(body)
                    .build();
        } else {
            request = new Request.Builder()
                    .url("https://alan-recipe-api.herokuapp.com/api/v1/favourite/"+recipeID)
                    .addHeader("Authorization", user.getToken())
                    .delete()
                    .build();
        }
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                if (response.isSuccessful()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (add) {
                                favourite = true;
                                favouriteButton.setText(R.string.removeFavourites);
                                Toast.makeText(RecipeDetailActivity.this, getString(R.string.addSuccessfully), Toast.LENGTH_LONG).show();
                            } else {
                                favourite = false;
                                favouriteButton.setText(R.string.addFavourites);
                                Toast.makeText(RecipeDetailActivity.this, getString(R.string.removeSuccessfully), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                } else if (response.code() == 400) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (add) {
                                Toast.makeText(RecipeDetailActivity.this, getString(R.string.addFailed), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(RecipeDetailActivity.this, getString(R.string.removeFailed), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
            }
        });
    }
}