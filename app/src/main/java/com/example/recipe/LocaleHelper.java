package com.example.recipe;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;

import java.util.HashMap;
import java.util.Locale;

public class LocaleHelper {
    public static final String ENGLISH = "en";
    public static final String TRADITIONAL_CHINESE = "zh_rHK";

    private static HashMap<String, Locale> languagesList = new HashMap<String, Locale>() {{
        put(ENGLISH, Locale.ENGLISH);
        put(TRADITIONAL_CHINESE, Locale.TRADITIONAL_CHINESE);
    }};

    public static void changeAppLanguage(Context context, String language) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();

        Locale locale = getLocaleByLanguage(language.equals("") ? ENGLISH : language);
        configuration.setLocale(locale);
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

    public static void changeAppLanguage(Context context, String language, Class<?> updateView) {
        changeAppLanguage(context, language);

        //Update view
        Intent intent = new Intent(context, updateView);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    private static Locale getLocaleByLanguage(String language) {
        if (languagesList.containsKey(language)) {
            return languagesList.get(language);
        } else {
            return Locale.ENGLISH;
        }
    }
}
