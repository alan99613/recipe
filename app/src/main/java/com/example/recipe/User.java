package com.example.recipe;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class User {
    private Context context;
    private SharedPreferences sharedPref;

    public User(Context context) {
        this.context = context;
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setToken(String token) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("token", "Bearer " + token);
        editor.apply();
    }

    public String getToken() {
        return sharedPref.getString("token", "");
    }

    public void removeToken() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove("token");
        editor.apply();
    }

    public void setLockApp(boolean lock) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("lockApp", lock);
        editor.apply();
    }

    public boolean getLockApp() {
        return sharedPref.getBoolean("lockApp", false);
    }

    public void setAppLanguage(String language) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("appLanguage", language);
        editor.apply();
    }

    public String getAppLanguage() {
        return sharedPref.getString("appLanguage", "");
    }
}
