package com.example.recipe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.recipe.adapter.RecipeAdapter;
import com.example.recipe.model.Recipe;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FavouriteActivity extends AppCompatActivity {
    private OkHttpClient client;
    private ListView favouriteListView;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);

        user = new User(this);
        //Check if the user is logged in
        if (user.getToken().equals("")) {
            Toast.makeText(FavouriteActivity.this, getString(R.string.notLogin), Toast.LENGTH_LONG).show();
            Intent intent = new Intent(FavouriteActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            client = new OkHttpClient();
            favouriteListView = findViewById(R.id.favouriteList);
            getFavourite();
        }
    }

    //Get favourites from API
    private void getFavourite() {
        Request request = new Request.Builder()
                .url("https://alan-recipe-api.herokuapp.com/api/v1/favourite")
                .addHeader("Authorization", user.getToken())
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                if (response.isSuccessful()) {
                    try {
                        List<Recipe> recipes = new LinkedList<>();
                        JSONArray recipeJson = new JSONArray(response.body().string());
                        for (int i = 0; i < recipeJson.length(); i++) {
                            String recipeID = recipeJson.getJSONObject(i).getString("_id");
                            String recipeName = recipeJson.getJSONObject(i).getString("recipeName");
                            String image = recipeJson.getJSONObject(i).getString("image");
                            recipes.add(new Recipe(recipeID, recipeName, image));
                        }
                        RecipeAdapter recipeAdapter = new RecipeAdapter(recipes, FavouriteActivity.this);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //Update the list view
                                favouriteListView.setAdapter(recipeAdapter);
                                favouriteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        Intent intent = new Intent(FavouriteActivity.this, RecipeDetailActivity.class);
                                        TextView recipeID = view.findViewById(R.id.recipe_child_id);
                                        intent.putExtra("recipeID", recipeID.getText().toString());
                                        startActivity(intent);
                                    }
                                });
                            }
                        });
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 401) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            user.removeToken();
                            Toast.makeText(FavouriteActivity.this, getString(R.string.authFailed), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(FavouriteActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
            }
        });
    }
}