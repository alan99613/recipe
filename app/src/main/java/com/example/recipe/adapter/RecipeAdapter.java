package com.example.recipe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.recipe.R;
import com.example.recipe.model.Recipe;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecipeAdapter extends BaseAdapter {
    private List<Recipe> recipeList;
    private Context context;

    public RecipeAdapter(List<Recipe> recipeList, Context context) {
        this.recipeList = recipeList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return recipeList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.recipe_item, parent, false);
        ImageView recipeImage = convertView.findViewById(R.id.recipe_child_image);
        TextView recipeID = convertView.findViewById(R.id.recipe_child_id);
        TextView recipeName = convertView.findViewById(R.id.recipe_child_enname);

        Picasso.get().load(recipeList.get(position).getImageUrl()).into(recipeImage);
        recipeID.setText(recipeList.get(position).getRecipeID());
        recipeName.setText(recipeList.get(position).getRecipeName());
        return convertView;
    }
}
