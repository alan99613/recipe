package com.example.recipe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.concurrent.Executor;

public class MainActivity extends AppCompatActivity {
    private User user;
    private Button lockAppButton;
    private static boolean checkLanguage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = new User(this);
        //Change the application language before setting the content view
        if (!checkLanguage) {
            checkLanguage = true;
            LocaleHelper.changeAppLanguage(this, user.getAppLanguage());
        }
        setContentView(R.layout.activity_main);

        //Change application language spinner
        Spinner appLanguageSpinner = findViewById(R.id.appLanguageSpinner);
        if (user.getAppLanguage().equals(LocaleHelper.TRADITIONAL_CHINESE)) {
            appLanguageSpinner.setSelection(1,false);
        } else {
            appLanguageSpinner.setSelection(0,false);
        }
        appLanguageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    changeLanguage(LocaleHelper.ENGLISH);
                } else {
                    changeLanguage(LocaleHelper.TRADITIONAL_CHINESE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        lockAppButton = findViewById(R.id.lockAppButton);
        //Check if the app is locked
        if (user.getLockApp()) {
            lockAppButton.setText(R.string.unlockApp);
            //Authenticated biometric or password
            BiometricPrompt biometricPrompt = createBiometricPrompt();
            BiometricPrompt.PromptInfo promptInfo = createPromptInfo();
            biometricPrompt.authenticate(promptInfo);
        } else {
            lockAppButton.setText(R.string.lockApp);
        }
        setDisplay();
    }

    //Change the application language and save the changed language
    private void changeLanguage(String language) {
        LocaleHelper.changeAppLanguage(this, language, MainActivity.class);
        user.setAppLanguage(language);
    }

    //Set the display of login or non-login UI
    private void setDisplay() {
        LinearLayout loggedIn = findViewById(R.id.loggedIn);
        LinearLayout notLoggedIn = findViewById(R.id.notLoggedIn);

        if (user.getToken().equals("")) {
            loggedIn.setVisibility(View.GONE);
            notLoggedIn.setVisibility(View.VISIBLE);
        } else {
            loggedIn.setVisibility(View.VISIBLE);
            notLoggedIn.setVisibility(View.GONE);
        }
    }

    public void recipePage(View view) {
        Intent intent = new Intent(MainActivity.this, ViewRecipeActivity.class);
        startActivity(intent);
    }

    public void favouritePage(View view) {
        Intent intent = new Intent(MainActivity.this, FavouriteActivity.class);
        startActivity(intent);
    }

    public void identifyFoodPage(View view) {
        Intent intent = new Intent(MainActivity.this, IdentificationActivity.class);
        startActivity(intent);
    }

    public void lockApp(View view) {
        if (user.getLockApp()) {
            user.setLockApp(false);
            lockAppButton.setText(R.string.lockApp);
        } else {
            user.setLockApp(true);
            lockAppButton.setText(R.string.unlockApp);
        }
    }

    public void registerPage(View view) {
        Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    public void loginPage(View view) {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    public void logout(View view) {
        user.removeToken();
        setDisplay();
        Toast.makeText(this, getString(R.string.logoutSuccessful), Toast.LENGTH_LONG).show();
    }

    private BiometricPrompt createBiometricPrompt() {
        Executor executor = ContextCompat.getMainExecutor(this);
        return new BiometricPrompt(this, executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                finishAndRemoveTask(); //If authentication fails, close the application
            }
        });
    }

    private BiometricPrompt.PromptInfo createPromptInfo() {
        return new BiometricPrompt.PromptInfo.Builder()
                .setTitle(getString(R.string.biometricTitle))
                .setSubtitle(getString(R.string.biometricSubtitle))
                .setDeviceCredentialAllowed(true)
                .build();
    }
}