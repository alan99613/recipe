package com.example.recipe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity {
    private OkHttpClient client;
    private EditText registerName, registerUsername, registerPassword, registerRPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        client = new OkHttpClient();

        registerName = findViewById(R.id.registerName);
        registerUsername = findViewById(R.id.registerUsername);
        registerPassword = findViewById(R.id.registerPassword);
        registerRPassword = findViewById(R.id.registerRPassword);
    }

    public void register(View view) {
        if (registerName.getText().toString().equals("") || registerUsername.getText().toString().equals("") || registerPassword.getText().toString().equals("") || registerRPassword.getText().toString().equals("")) {
            Toast.makeText(RegisterActivity.this, getString(R.string.registerEmpty), Toast.LENGTH_LONG).show();
        } else {
            if (registerPassword.getText().toString().equals(registerRPassword.getText().toString())) {
                registerHttp(registerName.getText().toString(), registerUsername.getText().toString(), registerPassword.getText().toString());
            } else {
                Toast.makeText(RegisterActivity.this, getString(R.string.passwordNotMatch), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void registerHttp(String name, String username, String password) {
        if (name.equals("") || username.equals("") || password.equals("")) {
            return;
        }

        //Json body
        JSONObject json = new JSONObject();
        try {
            json.put("name", name);
            json.put("username", username);
            json.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(String.valueOf(json), JSON);
        Request request = new Request.Builder()
                .url("https://alan-recipe-api.herokuapp.com/api/v1/register")
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                if (response.isSuccessful()) {
                    //Registration success
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(RegisterActivity.this, getString(R.string.registerSuccessful), Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
                } else if (response.code() == 422) {
                    //Registration failed
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(RegisterActivity.this, getString(R.string.usernameExist), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
            }
        });
    }
}