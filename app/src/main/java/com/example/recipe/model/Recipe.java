package com.example.recipe.model;

public class Recipe {
    private String recipeID, recipeName, description, imageUrl;
    private String[] ingredients, steps;

    public Recipe(String recipeID, String recipeName, String imageUrl) {
        this.recipeID = recipeID;
        this.recipeName = recipeName;
        this.imageUrl = imageUrl;
    }

    public Recipe(String recipeID, String recipeName, String description, String imageUrl, String[] ingredients, String[] steps) {
        this.recipeID = recipeID;
        this.recipeName = recipeName;
        this.description = description;
        this.imageUrl = imageUrl;
        this.ingredients = ingredients;
        this.steps = steps;
    }

    public String getRecipeID() {
        return recipeID;
    }

    public void setRecipeID(String recipeID) {
        this.recipeID = recipeID;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String[] getIngredients() {
        return ingredients;
    }

    public void setIngredients(String[] ingredients) {
        this.ingredients = ingredients;
    }

    public String[] getSteps() {
        return steps;
    }

    public void setSteps(String[] steps) {
        this.steps = steps;
    }
}
